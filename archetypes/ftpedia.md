---
hidden: true
layout: "issue"
title: "{{- $s := split .Name "-" }} {{- print (index $s 1) " / " (index $s 0)}}"
file: "ftpedia-{{ .Name }}.pdf"
publishDate: 
date: {{ .Date }}
uploadBy:
- "ft:pedia-Redaktion"
---
